package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Dmitriy on 01.05.2017.
 */
public class FindTextController implements Initializable {
    private static final char ZERO_SPACE = '\u200b';
    @FXML
    private Pane pane;
    @FXML
    private TextArea srcTextArea;
    @FXML
    private TextArea hiddenTextArea;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        srcTextArea.prefWidthProperty().bind(pane.widthProperty().multiply(0.95));
        srcTextArea.prefHeightProperty().bind(pane.heightProperty().multiply(0.45));
        srcTextArea.layoutXProperty().bind(pane.widthProperty().multiply(0.025));
        srcTextArea.layoutYProperty().bind(pane.heightProperty().multiply(0.033));

        hiddenTextArea.prefWidthProperty().bind(pane.widthProperty().multiply(0.95));
        hiddenTextArea.prefHeightProperty().bind(pane.heightProperty().multiply(0.45));
        hiddenTextArea.layoutXProperty().bind(pane.widthProperty().multiply(0.025));
        hiddenTextArea.layoutYProperty().bind(pane.heightProperty().multiply(0.516));

        srcTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            StringBuilder result = new StringBuilder();
            int count = 0;
            for (int i = 0; i < newValue.length(); i++) {
                if (newValue.charAt(i) == ZERO_SPACE) count++;
                if (newValue.charAt(i) != ZERO_SPACE) {
                    if (count != 0) {
                        result.append((char) count);
                        count = 0;
                    }
                }
            }
            if (count != 0) result.append((char) count);
            hiddenTextArea.setText(String.valueOf(result));
        });
    }
}
