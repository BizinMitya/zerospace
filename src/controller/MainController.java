package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {
    @FXML
    private FindTextController findTextController;
    @FXML
    private HideTextController hideTextController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
