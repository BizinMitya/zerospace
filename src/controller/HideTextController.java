package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Dmitriy on 01.05.2017.
 */
public class HideTextController implements Initializable {
    private static final char ZERO_SPACE = '\u200b';
    private static final int MAX_LENGTH = 1000;
    @FXML
    private Pane pane;
    @FXML
    private TextArea srcTextArea;
    @FXML
    private TextArea hideTextArea;
    @FXML
    private TextArea resTextArea;
    @FXML
    private ProgressBar progressBar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        srcTextArea.prefWidthProperty().bind(pane.widthProperty().multiply(0.95));
        srcTextArea.prefHeightProperty().bind(pane.heightProperty().multiply(0.3));
        srcTextArea.layoutXProperty().bind(pane.widthProperty().multiply(0.025));
        srcTextArea.layoutYProperty().bind(pane.heightProperty().multiply(0.025));

        hideTextArea.prefWidthProperty().bind(pane.widthProperty().multiply(0.95));
        hideTextArea.prefHeightProperty().bind(pane.heightProperty().multiply(0.29));
        hideTextArea.layoutXProperty().bind(pane.widthProperty().multiply(0.025));
        hideTextArea.layoutYProperty().bind(pane.heightProperty().multiply(0.35));

        progressBar.prefWidthProperty().bind(pane.widthProperty().multiply(0.95));
        progressBar.minHeightProperty().bind(pane.heightProperty().multiply(0.01));
        progressBar.maxHeightProperty().bind(pane.heightProperty().multiply(0.01));
        progressBar.layoutXProperty().bind(pane.widthProperty().multiply(0.025));
        progressBar.layoutYProperty().bind(pane.heightProperty().multiply(0.64));
        progressBar.setProgress(0);

        resTextArea.prefWidthProperty().bind(pane.widthProperty().multiply(0.95));
        resTextArea.prefHeightProperty().bind(pane.heightProperty().multiply(0.3));
        resTextArea.layoutXProperty().bind(pane.widthProperty().multiply(0.025));
        resTextArea.layoutYProperty().bind(pane.heightProperty().multiply(0.675));

        hideTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > MAX_LENGTH + 1)
                hideTextArea.setText(newValue.substring(0, MAX_LENGTH + 1));
            progressBar.setProgress(newValue.length() / (1d + srcTextArea.getText().length()));
            String srcText = srcTextArea.getText();
            String hideText = newValue.length() > MAX_LENGTH + 1 ? newValue.substring(0, MAX_LENGTH + 1) : newValue;
            if (hideText.length() <= srcText.length() + 1 && hideText.length() != 0) {
                StringBuilder resText = new StringBuilder();
                resText.append(getZeroSpacesByNumber(hideText.charAt(0)));
                for (int i = 0; i < srcText.length(); i++) {
                    resText.append(srcText.charAt(i));
                    if (hideText.length() > i + 1)
                        resText.append(getZeroSpacesByNumber(hideText.charAt(i + 1)));
                }
                resTextArea.setText(String.valueOf(resText));
            } else if (hideText.length() > srcText.length() + 1)
                hideTextArea.setText(newValue.substring(0, srcText.length() + 1));
            else if (hideText.length() == 0 && srcText.length() == 0) resTextArea.setText("");

        });

        srcTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > MAX_LENGTH)
                srcTextArea.setText(newValue.substring(0, MAX_LENGTH));
            progressBar.setProgress(hideTextArea.getText().length() / (1d + newValue.length()));
            String srcText = newValue.length() > MAX_LENGTH ? newValue.substring(0, MAX_LENGTH) : newValue;
            String hideText = hideTextArea.getText();
            StringBuilder newHideText = new StringBuilder();
            if (hideText.length() != 0) {
                StringBuilder resText = new StringBuilder();
                resText.append(getZeroSpacesByNumber(hideText.charAt(0)));
                newHideText.append(hideText.charAt(0));
                for (int i = 0; i < srcText.length(); i++) {
                    resText.append(srcText.charAt(i));
                    if (hideText.length() > i + 1) {
                        resText.append(getZeroSpacesByNumber(hideText.charAt(i + 1)));
                        newHideText.append(hideText.charAt(i + 1));
                    }
                }
                resTextArea.setText(String.valueOf(resText));
                hideTextArea.setText(String.valueOf(newHideText));
            } else if (hideText.length() == 0 && srcTextArea.getText().length() == 0) resTextArea.setText("");
            else if (hideText.length() == 0 && srcTextArea.getText().length() != 0)
                resTextArea.setText(srcTextArea.getText());
        });
    }

    /**
     * Метод получения строки, состоящей из count пробелов нулевой длины
     *
     * @param count число пробелов нулевой длины
     * @return строка и count пробелов нулевой длины
     */
    private String getZeroSpacesByNumber(int count) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < count; i++) {
            result.append(ZERO_SPACE);
        }
        return String.valueOf(result);
    }
}
